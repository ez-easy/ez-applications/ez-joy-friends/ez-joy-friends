from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Friend(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    joyful = db.Column(db.Boolean, default=False)
    imglink = db.Column(db.String(150))

def get_all_friends():
    return Friend.query.all()

def get_joyful_friends():
    return Friend.query.filter_by(joyful=True).all()

def get_unjoyful_friends():
    return Friend.query.filter_by(joyful=False).all()
