import pytest
from flask import Flask
from database import db, Friend, get_joyful_friends, get_unjoyful_friends, get_all_friends

@pytest.fixture
def app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    
    with app.app_context():
        db.create_all()

    yield app

    with app.app_context():
        db.session.remove()
        db.drop_all()

@pytest.fixture
def client(app):
    return app.test_client()

def test_get_friends_lists(app, client):
    with app.app_context():
        friend1 = Friend(name='Joey Tribbiani', joyful=True, imglink="https://upload.wikimedia.org/wikipedia/en/d/da/Matt_LeBlanc_as_Joey_Tribbiani.jpg")
        friend2 = Friend(name='Ross Geller', joyful=False, imglink="https://i0.wp.com/manforhimself.com/wp-content/uploads/2021/05/Ross-geller-swept-back-hair-1200-GettyImages-908307.jpg?fit=1200%2C1200&ssl=1")
        friend3 = Friend(name='Rachel Green', joyful=False, imglink="https://ik.imagekit.io/shortpedia/Voices/wp-content/uploads/2021/05/Rachel-Green-2.jpg")
        friend4 = Friend(name='Monica Geller', joyful=False, imglink="https://upload.wikimedia.org/wikipedia/en/d/d0/Courteney_Cox_as_Monica_Geller.jpg")
        friend5 = Friend(name='Chandler Bing', joyful=True, imglink="https://kaplan.co.uk/images/default-source/insights/chandler-bing.jpg")
        friend6 = Friend(name='Phoebe Buffay', joyful=True, imglink="https://www.looper.com/img/gallery/phoebe-buffays-friends-timeline-explained/intro-1621661137.jpg")

        db.session.add(friend1)
        db.session.add(friend2)
        db.session.add(friend3)
        db.session.add(friend4)
        db.session.add(friend5)
        db.session.add(friend6)
        db.session.commit()

        all_friends = get_all_friends()
        assert len(all_friends) == 6
        joyful_friends = get_joyful_friends()
        assert len(joyful_friends) == 3
        unjoyful_friends = get_unjoyful_friends()
        assert len(unjoyful_friends) == 3
